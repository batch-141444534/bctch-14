<?php

namespace App\Http\Controllers;

use App\Models\Batch;
use App\Models\Student;
use Illuminate\Http\Request;
use Image;
// use Intervention\Image\ImageManagerStatic as Image;


class StudentController extends Controller
{
    public function index(Request $request)
    {
        // dd($request->search);
        $studentsCollection = Student::latest();
        if($request->search){
            $serach = $request->search;
            $studentsCollection = Student::where('student_name','LIKE','%'.$serach.'%')->latest()
                                            ->orWhere('email','LIKE','%'.$serach.'%');
        }
        $students =  $studentsCollection->paginate(10);
        return view('backend.students.index', compact('students'));
    }
    public function create()
    {
        $batches = Batch::orderBy('batch_name', 'asc')
            ->pluck('batch_name', 'id')
            ->toArray();

        return view('backend.students.create', compact('batches'));
    }
    public function store(Request $request)
    {
        try{
            $request->validate([
                'student_name' => 'required|min:5',
                'email' => 'required',
                'phone_no' => 'required',
                'date_of_birth' => 'required',
                'gender' => 'required',
            ]);
            Student::create([
                'student_name' => $request->student_name,
                'email' => $request->email,
                'phone_no' => $request->phone_no,
                'date_of_birth' => $request->date_of_birth,
                'gender' => $request->gender,
                'batch_id' => $request->batch_id,
                'image' => $this->uploadImage(request()->file('image'))
            ]);
            return redirect()->route('students.index')->with('msg', 'Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

        
    }

    public function uploadImage($file)
    {
        // dd($file);
        if($file)
        {
            $file_name = time().'.'.$file->getClientOriginalExtension();
            Image::make($file)->save(public_path().'/images'.$file_name);
            // Image::make(storage_path().'/app/public/images'.$file_name);
            return $file_name;
        }
    } 

    public function show(Student $student)
    {
        // $student = Student::where('id', $id)->first();
        return view('backend.students.show', compact('student'));
    }

    public function edit(Student $student)
    {
        // $student = Student::where('id', $id)->first();
        return view('backend.students.edit', compact('student'));
    }
    public function update(Request $request, Student $student)
    {
        // $student = Student::where('id', $id)->first();
        $student->update([
            'student_name' => $request->student_name,
            'email' => $request->email,
            'phone_no' => $request->phone_no,
            'date_of_birth' => $request->date_of_birth,
            'gender' => $request->gender,
        ]);
        return redirect()->route('students.index');
    }
    
    
    public function destroy($id)
    {
        $student = Student::where('id', $id)->first();
        $student->delete();
        return redirect()->route('students.index');
    }
}

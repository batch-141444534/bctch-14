<?php

namespace App\Http\Controllers;

use App\Models\Batch;
use Illuminate\Http\Request;
// use Intervention\Image\ImageManagerStatic as Image;


class BatchController extends Controller
{
    public function index(Request $request)
    {
        // dd($request->search);
        $gendersCollection = Batch::latest();
        if($request->search){
            $serach = $request->search;
            $gendersCollection = Batch::where('batch_name','LIKE','%'.$serach.'%')->latest();
        }
        $batches =  $gendersCollection->paginate(10);

        return view('backend.batches.index', compact('batches'));
    }
    public function create()
    {
        return view('backend.batches.create');
    }
    public function store(Request $request)
    {
        try{
            $request->validate([
                'batch_name' => 'required|min:3',
            ]);
            Batch::create([
                'batch_name' => $request->batch_name,
            ]);
            return redirect()->route('batches.index')->with('msg', 'Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

        
    }

    public function show(Batch $batch)
    {
        // $batch = Batch::where('id', $id)->first();
        $students = $batch->student;
        return view('backend.batches.show', compact('batch', 'students'));
    }

    public function edit(Batch $batch)
    {
        // $batch = Batch::where('id', $id)->first();
        return view('backend.batches.edit', compact('batch'));
    }
    public function update(Request $request, Batch $batch)
    {
        // $batch = Batch::where('id', $id)->first();
        $batch->update([
            'batch_name' => $request->student_name,
        ]);
        return redirect()->route('batches.index');
    }
    
    
    public function destroy($id)
    {
        $batch = Batch::where('id', $id)->first();
        $batch->delete();
        return redirect()->route('batches.index');
    }
}

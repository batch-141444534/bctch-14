<x-backend.layouts.master>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('students.store') }}" method="post"  enctype="multipart/form-data">
        @csrf

        
        <select name="batch_id" id="batch_id" class="form-select">
            @foreach ($batches as $key=>$value)
            <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>


        <label for="student_name">Student Name</label>
        <input type="text" name="student_name"><br>

        <label for="email">Email</label>
        <input type="text" name="email"><br>
        <label for="phone_no">Phone No</label>
        <input type="text" name="phone_no"><br>
        <label for="date_of_birth">Date of Birth</label>
        <input type="date" name="date_of_birth"> <br>
        <label for="gender">Gender</label><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <label for="email">Upload a Image</label>
        <input type="file" name="image"><br>
        <button type="submit">Save</button>
    </form>
</x-backend.layouts.master>

<x-backend.layouts.master>

    <form action="{{route('students.update', $student->id)}}" method="post">
        @csrf
        @method('PATCH')
        <label for="student_name">Student Name</label>
        <input type="text" name="student_name" value="{{$student->student_name}}"><br>    
        <label for="email">Email</label>    
        <input type="text" name="email" value="{{$student->email}}"><br>    
        <label for="phone_no">Phone No</label>    
        <input type="text" name="phone_no" value="{{$student->phone_no}}"><br>   
        <label for="date_of_birth">Date of Birth</label>    
        <input type="date" name="date_of_birth" value="{{$student->date_of_birth}}"> <br>   
        <label for="gender">Gender</label><br>    
        <input type="radio" id="male" name="gender" value="male" @if($student->gender === "male") checked @endif>
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female" @if($student->gender === "female") checked @endif>
        <label for="female">Female</label><br>   
        <button type="submit">Update</button>   
    </form>

</x-backend.layouts.master>
<x-backend.layouts.master>

    Name: {{$student->student_name}} <br>
    Batch: <a href="{{route('batches.show', $student->batch->id)}}">{{$student->batch->batch_name}} </a> <br>
    Email: {{$student->email}} <br>
    Phone No: {{$student->phone_no}} <br>
    Date of Birth: {{$student->date_of_birth}} <br>
    Gender: {{$student->gender}}

</x-backend.layouts.master>
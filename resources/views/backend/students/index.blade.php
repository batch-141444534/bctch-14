<x-backend.layouts.master>
    @if (\Session::has('msg'))
    <div class="alert alert-primary" role="alert">
        {!! \Session::get('msg') !!}
    </div>
    @endif
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Students
            <table style="width: 100%; border-collapse: collapse" border="1">
                <thead>
                    <tr>
                        <th colspan="5">Students Table</th>
                        <th>
                            <form action="{{route('students.index')}}" method="get">
                                <input type="text" for='search' name='search' placeholder="Search">
                                {{-- <i class="ph-magnifying-glass opacity-50" aria-hidden="true"></i> --}}
                                <button type="submit"><i class="fas fa-search"></i></button>
                            </form>
                        </th>
                        <th><button><a href="{{route('students.create')}}">Add Student</a></button></th>
                    </tr>
                    <tr>
                        <th>Ser No</th>
                        <th>Name</th>
                        <th>Batch</th>
                        <th>Email</th>
                        <th>Phone No</th>
                        <th>Date of Birth</th>
                        <th>Gender</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $sl = 1
                    @endphp
                    @foreach ($students as $key => $student)
                    <tr>
                        <td>{{$sl++}}</td>
                        <td>{{$student->student_name}}</td>
                        <td>{{$student->batch->batch_name}}</td>
                        <td>{{$student->email}}</td>
                        <td>{{$student->phone_no}}</td>
                        <td>{{$student->date_of_birth}}</td>
                        <td>{{$student->gender}}</td>
                        <td>
                            <button><a href="{{route('students.show', $student->id)}}">Show</a></button>
                            <button><a href="{{route('students.edit', $student->id)}}">Edit</a></button>
                            <form action="{{route('students.delete', $student->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- <div class="card-footer">
            {{$students->links()}}
        </div> --}}
    </div>
</x-backend.layouts.master>
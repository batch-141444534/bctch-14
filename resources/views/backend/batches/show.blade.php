<x-backend.layouts.master>

    Name: {{$batch->batch_name}} <br>
    {{__('Student List:')}} 
    <table>
        <thead><tr><th>Student Name</th></tr></thead>
        @foreach ($students as $student)
            <tbody><tr><td><a href="{{route('students.show', $student->id)}}">{{$student->student_name}}</a></td></tr></tbody>
        @endforeach
    </table>

</x-backend.layouts.master>
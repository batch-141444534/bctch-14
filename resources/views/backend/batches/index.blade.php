<x-backend.layouts.master>
    @if (\Session::has('msg'))
    <div class="alert alert-primary" role="alert">
        {!! \Session::get('msg') !!}
    </div>
    @endif
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Batch List 
            <table style="width: 100%; border-collapse: collapse" border="1">
                <thead>
                    <tr>
                        <th>Batch Table</th>
                        <th>
                            <form action="{{route('batches.index')}}" method="get">
                                <input type="text" for='search' name='search' placeholder="Search">
                                {{-- <i class="ph-magnifying-glass opacity-50" aria-hidden="true"></i> --}}
                                <button type="submit"><i class="fas fa-search"></i></button>
                            </form>
                        </th>
                        <th><button><a href="{{route('batches.create')}}">Add Batch</a></button></th>
                    </tr>
                    <tr>
                        <th>Ser No</th>
                        <th>Batch Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $sl = 1
                    @endphp
                    @foreach ($batches as $key => $batch)
                    <tr>
                        <td>{{$sl++}}</td>
                        <td>{{$batch->batch_name}}</td>
                        <td>
                            <button><a href="{{route('batches.show', $batch->id)}}">Show</a></button>
                            <button><a href="{{route('batches.edit', $batch->id)}}">Edit</a></button>
                            <form action="{{route('batches.delete', $batch->id)}}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- <div class="card-footer">
            {{$batches->links()}}
        </div> --}}
    </div>
</x-backend.layouts.master>
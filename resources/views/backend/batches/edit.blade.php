<x-backend.layouts.master>

    <form action="{{route('batches.update', $batch->id)}}" method="post">
        @csrf
        @method('PATCH')
        <label for="batch_name">Batch Name</label>
        <input type="text" name="batch_name" value="{{$batch->batch_name}}"><br>      
        <button type="submit">Update</button>   
    </form>

</x-backend.layouts.master>
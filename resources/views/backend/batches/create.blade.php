<x-backend.layouts.master>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('batches.store') }}" method="post"  enctype="multipart/form-data">
        @csrf
        <label for="batch_name">Batch Name</label>
        <input type="text" name="batch_name"><br>
        <button type="submit">Save</button>
    </form>
</x-backend.layouts.master>

<!DOCTYPE html>
<html lang="en">
<x-backend.layouts.partials.head/>

<body>
<x-backend.layouts.partials.navbar/>
   
    <div class="page-content">
<x-backend.layouts.partials.sidebar/>

        <div class="content-wrapper">
            <div class="content">

               {{$slot}}

            </div>
            @php
                $webName = "www.google.com";
            @endphp
            <x-backend.layouts.partials.footer :webName="$webName"/>
        </div>
    </div>
</body>

</html>
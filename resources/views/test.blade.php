<form action="test" method="post">
    @csrf
    <table>
        <tr>
            <td>Name</td>
            <td colspan="3"><input type="text" name="name"></td>
        </tr>
        <tr>
            <td>Qualification</td>
            <td><input type="text" name="edu['ssc']"></td>
            <td><input type="text" name="edu['hsc']"></td>
            <td><input type="text" name="edu['bsc']"></td>
        </tr>
        <tr>
            <td><button type="submit">Save</button></td>
        </tr>
    </table>
</form>